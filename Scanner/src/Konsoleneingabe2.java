import java.util.Scanner;

public class Konsoleneingabe2 {

	public static void main(String[] args) 
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Hallo, bitte sag mir deinen Namen: ");
		
		String Name = myScanner.nextLine();
		System.out.print("bitte sag mir deine Alter: ");
		
		String Age = myScanner.nextLine();

		System.out.print("Dein Name ist " + Name + "und du bist " + Age + " Jahre alt.");
	}

}
